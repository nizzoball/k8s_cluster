# K8s_Cluster

Steps to install a triple master k8s cluster

## Create DNS records
We need to create DNS records for 

kubemaster1
kubemaster2
kubemaster3

kubeworker1
kubeworker2
kubeworker3

kubevip (Virtual IP that will be provided by keepalived)

The kubevip can be anything on the subnet that is addressible.  Just make sure it doesn't conflict with something else, this IP will only be active when/if keepalived is running and will be configured via keepalived

# Start on kubemaster1
## Install cfssl

`wget https://pkg.cfssl.org/R1.2/cfssl_linux-amd64`

`wget https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64`

`chmod +x cfssl*`

`sudo mv cfssl_linux-amd64 /usr/local/bin/cfssl`

`sudo mv cfssljson_linux-amd64 /usr/local/bin/cfssljson`

`sudo chown root:root /usr/local/bin/cfssl*`

## Create the etcd certs
### Create the CA certs

`vim ca-config.json`

Paste the following json into ca-config.jsen

```json
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
```


`vim ca-csr.json`

Paste the following json into ca-csr.json

```json
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
  {
    "C": "IE",
    "L": "Cork",
    "O": "Kubernetes",
    "OU": "CA",
    "ST": "Cork Co."
  }
 ]
}
```

Generate the cert authority and private key

`cfssl gencert -initca ca-csr.json | cfssljson -bare ca`

You should now have ca-key.pem and ca.pem files

### Create the etcd certs

`vim kubernetes-csr.json`

Paste the following json into kubernetes-csr.json

```json
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
  {
    "C": "IE",
    "L": "Cork",
    "O": "Kubernetes",
    "OU": "Kubernetes",
    "ST": "Cork Co."
  }
 ]
}
```

Generate the certificate and private key

```
cfssl gencert \
-ca=ca.pem \
-ca-key=ca-key.pem \
-config=ca-config.json \
-hostname=10.10.40.90,kubemaster1,10.10.40.91,kubemaster2,10.10.40.92,kubemaster3,10.10.40.93,kubevip,kubernetes.default \
-profile=kubernetes kubernetes-csr.json | \
cfssljson -bare kubernetes
```

In the above command the IPs are
10.10.40.90 = kubemaster1 IP
10.10.40.91 = kubemaster2 IP
10.10.40.92 = kubemaster3 IP
10.10.40.93 = kubevip IP

We put both the IP and hostname in so no matter how it gets addressed, SSL will work with etcd

You should now have a cert called kubernetes.pem

### Copy the certs to each master/node

`scp ca.pem kubernetes.pem kubernetes-key.pem ec2-user@kubemaster2:`

`scp ca.pem kubernetes.pem kubernetes-key.pem ec2-user@kubemaster3:`

`scp ca.pem kubernetes.pem kubernetes-key.pem ec2-user@kubeworker1:`

`scp ca.pem kubernetes.pem kubernetes-key.pem ec2-user@kubeworker2:`

`scp ca.pem kubernetes.pem kubernetes-key.pem ec2-user@kubeworker3:`

## Install and configure keepalived
### IMPORTANT!!! Only install keepalived and HA Proxy on the primary master right now.

`sudo yum install keepalived`

`sudo vim /etc/keepalived/keepalived.conf`

Paste in

```
vrrp_script haproxy-check {
    script "killall -0 haproxy"
    interval 2
    weight 20
}
 
vrrp_instance haproxy-vip {
    state BACKUP
    priority 101
    interface eth0
    virtual_router_id 47
    advert_int 3
 
    unicast_src_ip 10.10.40.90 
    unicast_peer {
        10.10.40.91
        10.10.40.92 
    }
 
    virtual_ipaddress {
        10.10.40.93 
    }
 
    track_script {
        haproxy-check weight 20
    }
}
```

In the above config file the IPs are
* 10.10.40.90 = kubemaster1 IP
* 10.10.40.91 = kubemaster2 IP
* 10.10.40.92 = kubemaster3 IP
* 10.10.40.93 = kubevip IP


The config file above will be also put on kubemaster2 and 3 but the IP's will be shuffled around.  "unicast_src_ip" is the IP of the machine that is running keepalived (so will be the IP of whatever machine it's on), while the "unicast_peer" IP's are the IPs of the other two masters.

`sudo yum install haproxy`

`sudo vim /etc/haproxy/haproxy.cfg`

Paste in

```
frontend k8s-api
  bind 10.10.40.93:443
  mode tcp
  option tcplog
  default_backend k8s-api

backend k8s-api
  mode tcp
  option tcplog
  option tcp-check
  balance roundrobin
  default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100
  server k8s-api-1 10.10.40.90:6443 check
  server k8s-api-2 10.10.40.91:6443 check
  server k8s-api-3 10.10.40.92:6443 check
```

In the above config file the IPs are
* 10.10.40.90 = kubemaster1 IP
* 10.10.40.91 = kubemaster2 IP
* 10.10.40.92 = kubemaster3 IP
* 10.10.40.93 = kubevip IP


Start/restart keepalived

`sudo systemctl start keepalived`

The kubevip should be pingable now

## Install and configure etcd

This can be done simultaneously on the three masters

`sudo mkdir /etc/etcd /var/lib/etcd`

`sudo mv ~/ca.pem ~/kubernetes.pem ~/kubernetes-key.pem /etc/etcd`

`wget https://github.com/coreos/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz`

`tar -xvzf etcd-v3.3.9-linux-amd64.tar.gz`

`sudo mv etcd-v3.3.9-linux-amd64/etcd* /usr/local/bin/`

`sudo chown root:root /usr/local/bin/etcd*`

Create a systemd unit file for etcd

`sudo vim /etc/systemd/system/etcd.service`

```
[Unit]
Description=etcd
Documentation=https://github.com/coreos


[Service]
ExecStart=/usr/local/bin/etcd \
  --name 10.10.40.90 \
  --cert-file=/etc/etcd/kubernetes.pem \
  --key-file=/etc/etcd/kubernetes-key.pem \
  --peer-cert-file=/etc/etcd/kubernetes.pem \
  --peer-key-file=/etc/etcd/kubernetes-key.pem \
  --trusted-ca-file=/etc/etcd/ca.pem \
  --peer-trusted-ca-file=/etc/etcd/ca.pem \
  --peer-client-cert-auth \
  --client-cert-auth \
  --initial-advertise-peer-urls https://10.10.40.90:2380 \
  --listen-peer-urls https://10.10.40.90:2380 \
  --listen-client-urls https://10.10.40.90:2379,http://127.0.0.1:2379 \
  --advertise-client-urls https://10.10.40.90:2379 \
  --initial-cluster-token etcd-cluster-0 \
  --initial-cluster 10.10.40.90=https://10.10.40.90:2380,10.10.40.91=https://10.10.40.91:2380,10.10.40.92=https://10.10.40.92:2380 \
  --initial-cluster-state new \
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5



[Install]
WantedBy=multi-user.target
```

In the above config file the IPs are
* 10.10.40.90 = kubemaster1 IP
* 10.10.40.91 = kubemaster2 IP
* 10.10.40.92 = kubemaster3 IP


This unit file will need to be changed to fit the machine that it's on

`sudo systemctl daemon-reload`

`sudo systemctl enable etcd`

`sudo systemctl start etcd`

Repeat for the other two masters with the modified unit files

After etcd is started on all three you should get healthy returned from the following command

`etcdctl --endpoints "https://10.10.40.92:2379" --ca-file /etc/etcd/ca.pem --cert-file /etc/etcd/kubernetes.pem --key-file /etc/etcd/kubernetes-key.pem cluster-health`

## Disable swap

Kubernetes wont run if swap is on.  Disable swap on all machines

`sudo swapoff -a`

`sudo sed -i '/ swap / s/^/#/' /etc/fstab`

## Install Docker and Kubernetes on kubemaster

Need to enable docker repo

`sudo yum install yum-utils`

`sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo`



Need to enable to kubernetes repo on RHEL

`sudo touch /etc/yum.repos.d/kubernetes.repo`

Paste the repo info

`sudo vim /etc/yum.repos.d/kubernetes.repo`

```
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

```

If firewalld is running, stop it and disable it or make sure ports 6443 and 10250 are open
Doing that is outside the scope of this guide


`sudo yum install docker-ce`

`sudo yum install kubelet kubeadm kubectl`

`sudo systemctl enable docker && sudo systemctl start docker`

`sudo systemctl enable kubelet`

#### We need to set docker to use systemd driver

`sudo vim /usr/lib/systemd/system/docker.service`

At the end of the "ExecStart=/usr/bin/dockerd" line add

`--exec-opt native.cgroupdriver=systemd`

Reload and restart docker

`sudo systemctl daemon-reload`

`sudo systemctl restart docker`

Copy the repo file to the other masters and workers

## Initialize the first master as a kubemaster

Create an init config

`vim config.yaml`

Paste in the kube config

```
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: stable
apiServer:
  certSANs:
  - 10.10.40.93
  extraArgs:
    apiserver-count: "3"
controlPlaneEndpoint: "10.10.40.93:6443"
etcd:
  external:
    endpoints:
    - https://10.10.40.90:2379
    - https://10.10.40.91:2379
    - https://10.10.40.92:2379
    caFile: /etc/etcd/ca.pem
    certFile: /etc/etcd/kubernetes.pem
    keyFile: /etc/etcd/kubernetes-key.pem
networking:
  podSubnet: 10.30.0.0/24
```

In the above config file the IPs are
* 10.10.40.90 = kubemaster1 IP
* 10.10.40.91 = kubemaster2 IP
* 10.10.40.92 = kubemaster3 IP
* 10.10.40.93 = kubevip IP

`kubeadm init --config=config.yaml`

#### Note: If you have less than 2 CPUs kubeadm will fail on pre-flight. If this is the case then run the following command

`kubeadm init --config=config.yaml --ignore-preflight-errors=NumCPU`

Copy the certs to the other two masters

`sudo cp -r /etc/kubernetes/pki ~`

`sudo chown -R ec2-user:ec2-user ~/pki`

`scp -r ~/pki ec2-user@kubeworker2:~`

`scp -r ~/pki ec2-user@kubeworker3:~`

Copy the config.yaml to kubemaster 2 and 3

`scp config.yaml ec2-user@kubemaster2:`

`scp config.yaml ec2-user@kubemaster3:`

## Configure kubemaster2

Login to kubemaster2
Reconoiter the certs

`rm ~/pki/apiserver.*`

`mv ~/pki /etc/kubernetes`

`yum install docker`

`yum install kubelet kubeadm kubectl`

`kubeadm init --config=config.yaml`


## Configure kubemaster3

Login to kubemaster3
Reconoiter the certs

`rm ~/pki/apiserver.*`

`mv ~/pki /etc/kubernetes`

`yum install docker`

`yum install kubelet kubeadm kubectl`

`kubeadm init --config=config.yaml`


## Install and configure keepalived and haproxy on kubemaster 2 and 3

Do the needful

Make sure the IP's are reconoitered to fit with the machines they are on

Start keepalived on master 2 then master 3


# Install and configure the worker nodes

The following can be done on all three workers at the same time

`yum install docker`

`yum install kubeadm kubelet kubectl`

On one of the master nodes get the token and join command

`kubeadm token create --print-join-command`

Run the command that is output on each of the workers

## Configure the kubemasters so you can use kubectl right

On kubemaster1

Make sure you're not root

`mkdir ~/.kube`

`sudo cp /etc/kubernetes/admin.conf ~/.kube/config`

`sudo chown ec2-user:ec2-user ~/.kube/config`

`chmod 600 ~/.kube/config`

Repeat on kubemaster2 and 3


You should now be able to run kubectl commands against the cluster

`kubectl get nodes`

The output will probably say the status is not ready, thats because we need an overlay network which we're going to do now


## Deploy the overlay network

`kubectl apply -f https://git.io/weave-kube-1.6`

It will take a couple minutes for the weave pods to come up, when they do everything should read as "Ready"

You now have a working multi-master kubernetes cluster fully contained within 6 machines